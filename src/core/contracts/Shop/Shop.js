import { ethers, provider } from '@/core/core'
const ContractArtifact = require('@/core/contracts/Shop/Shop.json')
const CONTRACT_ADDRESS = '0x5fbdb2315678afecb367f032d93f642f64180aa3'
let readOnlyContract = new ethers.Contract(CONTRACT_ADDRESS, ContractArtifact.abi, provider)
export default {
    async createPayment(clientID, orderID, sum) {
        const contractSigner = await this.getConrtactSigner()
        const txResponse = await contractSigner.createPayment(clientID, orderID, {
            value: ethers.utils.parseEther(sum.toString()),
            gasLimit: 300000
        })
        await txResponse.wait()

        return { message: "payed" }
    },
    async withdrawAll() {
        const contractSigner = await this.getConrtactSigner()
        const txResponse = await contractSigner.withdrawAll()
        await txResponse.wait()
        return { message: "withdrawAll" }
    },
    async getConrtactSigner() {
        const signer = provider.getSigner()
        let contract = new ethers.Contract(CONTRACT_ADDRESS, ContractArtifact.abi, signer)
        let contractSigner = contract.connect(signer)
        return contractSigner
    },
    async getClientBalance() {
        const signer = provider.getSigner()
        const address = await signer.getAddress()
        const balance = await provider.getBalance(address)
        return ethers.utils.formatEther(balance)
    },
    async getContractBalance() {
        const balance = await provider.getBalance(CONTRACT_ADDRESS)
        return ethers.utils.formatEther(balance)
    },
    listenEvents() {
        provider.once("block", () => {
            readOnlyContract.on("newPayment", (clientAddress, clientID, orderID, sum, date) => {
                var date2 = date.toNumber()
                var date1 = new Date(date2 * 1000);
                console.log(clientAddress, clientID.toString(), orderID.toString(), sum.toString(), date1.toUTCString());
            });
        });

    },
}