// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.9;

contract Shop {
    address owner;

    constructor() {
        owner = msg.sender;
    }

    modifier onlyOwner() {
        require(msg.sender == owner, "Access denied!");
        _;
    }

    event newPayment(address indexed _clientAddress, uint indexed _clientID, uint indexed _orderID, uint _sum, uint _date);

    function createPayment(uint _clientID, uint _orderID) public payable {
        emit newPayment(msg.sender, _clientID,  _orderID, msg.value, block.timestamp);
    }

    function withdrawAll() external onlyOwner {
        address payable _to = payable(owner);
        address _thisContract = address(this);
        _to.transfer(_thisContract.balance);
    }
}
