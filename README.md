# blockchain test vue

Contract: src\core\contracts\Shop
1. Deploy contract to test network
2. Change CONTRACT_ADDRESS in src\core\contracts\Shop\Shop.js

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
